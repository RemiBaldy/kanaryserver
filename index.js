// import express
const express = require('express');
const cors = require('cors');
const https = require('https');
const fs = require('fs');
var path = require('path');

let apys = require('../../updater/apicompute/data/apys.json');
let lpPrices = require('../../updater/apicompute/data/lpPrices.json');

fs.watchFile('../../updater/apicompute/data/apys.json', (curr, prev) => {
  var filename = path.resolve('../../updater/apicompute/data/apys.json');
  delete require.cache[filename];
  apys = require('../../updater/apicompute/data/apys.json');
});

fs.watchFile('../../updater/apicompute/data/lpPrices.json', (curr, prev) => {
  var filename = path.resolve('../../updater/apicompute/data/lpPrices.json');
  delete require.cache[filename];
  lpPrices = require('../../updater/apicompute/data/lpPrices.json');
});


// create new express app and assign it to `app` constant
const app = express();

// use middleware
app.use(cors());

// server port configuration
const PORT = 1234;

// create a route for the app
app.get('/', (req, res) => {
  res.send('Hello!');
});

app.get('/apys', (req, res) => {
  res.send(apys);
});

app.get('/lpPrices', (req, res) => {
  res.send(lpPrices);
});


const httpsServer = https.createServer({
  key: fs.readFileSync('../certif/privatekey.pem'),
  cert: fs.readFileSync('../certif/certificate.pem'),
}, app);


httpsServer.listen(PORT, () => {
    console.log(`HTTPS Server running https://localhost:${PORT}/`);
});

  
